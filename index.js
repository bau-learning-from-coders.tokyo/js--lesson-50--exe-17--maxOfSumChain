function maxOfSumChain(arr, k) {
	let temp = 0;
	for (i = 0; i < arr.length - k + 1; i++) {
		let tempInLoop = 0;
		for (j = i; j < i + k; j++) {
			tempInLoop += arr[j];
		}

		if (temp < tempInLoop) temp = tempInLoop;
	}

	return temp;
}

console.log(maxOfSumChain([1, 3, 2], 2));
console.log(maxOfSumChain([1, 3, 2, 6, 2], 3));
